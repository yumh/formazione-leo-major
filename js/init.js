const on_localhost = window.location.href.indexOf("localhost") > -1;

document.addEventListener('DOMContentLoaded', () => {
    if (on_localhost) {
        const socket = io();
    }

    Reveal.initialize({
        history: true,
        controls: true,
        // transition: 'convex', // none/fade/slide/convex/concave/zoom
        transition: 'slide', // none/fade/slide/convex/concave/zoom

        slideNumber: 'c/t',

        dependencies: [
            // Interpret Markdown in <section> elements
            { src: 'plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
            { src: 'plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },

            // Syntax highlight for <code> elements
            { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },


            // Speaker notes
            { src: 'plugin/notes/notes.js', async: true },
        ]
    });

    if (on_localhost) {
        const sendState = e => {
            const { v, h, f } = Reveal.getIndices();
            // socket.emit('slidechanged', e);
            socket.emit('slidechanged', {v, h, f});
        };

        Reveal.addEventListener('slidechanged', sendState);
        Reveal.addEventListener('fragmentshown', sendState);
        Reveal.addEventListener('fragmenthidden', sendState);

        socket.on('slidechanged', e => {
            console.log(e);
            Reveal.slide(e.h, e.v, e.f);
        });
    }
});
